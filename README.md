# README #

A place to practise git branch

### What is this repository for? ###

* Demonstrating useful Git command
* v1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

- Install [Git](https://git-scm.com/downloads)
- vscode 
- vscode extension: [git graph](https://www.youtube.com/watch?v=u9ZQpKGTog4)
    - vscode keyboard shortcut (Ctrl+k Ctrl+S)
    - git graph view git log (Ctrl+Shift+B)
- [avoid passphrase](https://superuser.com/questions/988185/how-to-avoid-being-asked-enter-passphrase-for-key-when-im-doing-ssh-operatio)
    - eval `ssh-agent -s`
    - `ssh-add /c/Users/etan221/.ssh/id_rsa`
    - `ssh-add -l`
    - `ssh bitbucket.org`
    - permanent solution: A slightly better and permanent solution is to auto launch the ssh-agent when opening the git bash on windows. You can copy/paste the below in your .profile or .bashrc. I prefer to put it on the .profile

```
env=~/.
env=~/.ssh/agent.env
agent_load_env () { test -f "$env" && . "$env" >| /dev/null ; }
agent_start () {
    (umask 077; ssh-agent >| "$env")
    . "$env" >| /dev/null ; }
agent_load_env
# agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)
if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
    agent_start
    ssh-add
elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
    ssh-add
fi
unset env
```
### Who do I talk to? ###

* Eric Tan, etan221
* eric.tan@auckland.ac.nz

### Git Command used

- branch:
    - `git checkout -b feature/RSM-111-add-login`
    - `git branch`

- add:
    - `git add .`
    - `git commit -m "m1"`
    - `git status`
    - `git log`

- [rebase](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)
    - Both of these commands are designed to integrate changes from one branch into another branch—they just do it in very different ways.
    - `git checkout feature/RSM-111-add-login`
    - `git rebase master`

- [stash](https://www.atlassian.com/git/tutorials/saving-changes/git-stash)
    - `git stash save "WIP feature/RSM-111 a`
    - `git stash list`
    - `git stash show`, `git stash show -p`
    - `git stash apply`, `git stash pop`
    - `git stash drop stash@{1}`

- push:
    - `git push -u origin feature/RSM-111-add-login`

- [revert](https://www.atlassian.com/git/tutorials/undoing-changes/git-revert)
    - `git log --oneline`
    - `git checkout <commitHash>`
    - enahnce the change
    - `git add .`, `git commit -m "XXX enhanced"`
    - `git revert <commitHash>`

- [reset](https://www.atlassian.com/git/tutorials/undoing-changes/git-reset)
    - `git reset --soft HEAD~1` back 1 commit but keep the code or `git reset --hard HEAD~1` back 1 commit and drop the code
    - `git add .`, `git commit -m "fix something"` fix
    - `git push origin -d feature/RSM-333-dashboard` delete remote feature branch
    - `git push origin feature/RSM-333-dashboard` push local feature branch

- [prune](https://www.atlassian.com/git/tutorials/git-prune) after pull request:
    - `git remote prune origin` remove remote branch reference
    - `git checkout master`
    - `git branch -d <featureBranch>`, `git branch -D <featureBranch>` remove local branch
    - `git pull` update master branch

### Getting Problem
- [You have not concluded your merge (MERGE_HEAD exists)](https://tecadmin.net/you-have-not-concluded-your-merge-merge_head-exists/)
- [Tips and Tricks](https://tecadmin.net/category/git-tips-tricks/)